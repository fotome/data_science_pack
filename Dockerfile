FROM python:3.7
ENV PYTHONUNBUFFERED 1
ENV DEBIAN_FRONTEND noninteractive

# install japanese font
RUN apt-get update
RUN apt-get install -y fonts-noto-cjk
RUN apt-get install -y texlive texlive-latex-extra texlive-xetex texlive-lang-japanese pandoc nodejs npm

# make working dir
RUN mkdir /src
WORKDIR /src

# python
COPY src/requirements.txt /src/requirements.txt
RUN pip install --upgrade pip
RUN pip install -r requirements.txt
RUN pip install jupyterlab pandas numpy scipy scikit-learn matplotlib
RUN pip install tensorflow keras pillow
ADD src/ /src/

# matplotlib config
RUN mkdir /root/.cache/matplotlib
RUN cp /src/.fontlist-v310.json /root/.cache/matplotlib/fontlist-v310.json

### pdf export config
RUN cp /src/base.tplx /usr/local/lib/python3.7/site-packages/nbconvert/templates/latex/base.tplx