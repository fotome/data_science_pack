# データサイエンス向けdockerパッケージ


## about
データサイエンス向けのpython環境です。  
numpy, scipy, pandasがプリインストールされています。

## usage
```
docker-compose build
docker-compose up (-d)
```
起動後、コンソールに表示されているurlにブラウザからアクセスしてください。
```
http://localhost:8888/?token=<access_token>
```


### bash
```
docker-compose exec app bash
```
